#include<stdio.h>
//Abraham Hernandez Medina 
int main()
{
  	int ciclo, numero;
  	int guarda = 0;
 
  	do{
  	    printf("\n Ingresa un numero entre 1 y 50: ");
  	    scanf("%d", &numero);
  	}while(numero>50);
  	
  	printf("\n La suma de numeros consecutivos entre el 1 y el %d es:", numero);
  	for(ciclo = 1; ciclo <= numero; ciclo++)
  	{
    		guarda = guarda + ciclo;
    	}
  	printf("\n %d", guarda);
	return 0;
}