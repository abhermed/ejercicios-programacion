Inicio
    Variable ciclo es Numérico Real
    Variable numero es Numérico Real
    Variable guarda es Numérico Real

    ciclo = 1
    guarda = 0

    Hacer
        Leer "Ingresa un numero entre el 1 y el 50", numero
    Mientras(numero>50) //Para validar que esté en rango
    
    Escribir "La suma de numeros consecutivos entre el 1 y el numero es:"
    Para(ciclo = 1, ciclo <= numero, ciclo = ciclo+1)
	guarda = guarda + ciclo
    Fin-Para
    Escribir guarda
Fin