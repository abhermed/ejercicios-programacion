#include <stdio.h>
//Abraham Hernández Medina

int main(){
   float lado;
   float perimetro;
   
   printf("Ingresa la medida de la base del triangulo: ");
   scanf("%f", &lado);
   
   perimetro = (3 * lado);
   
   printf("El perimetro es %f\n", perimetro);
   return 0;
}