#include <stdio.h>
//Abraham Hernandez Medina
int main(){
   float ladoA;
   float ladoB;
   float perimetro;
   
   printf("Ingresa la medida de la base del triangulo: ");
   scanf("%f", &ladoB);
   
   printf("Ingresa la medida de un lado  del triangulo: ");
   scanf("%f", &ladoA);
   
   perimetro = (2 * ladoA) + ladoB;
   
   printf("El perimetro es %f\n", perimetro);
   return 0;
}