#include <stdio.h>
//Abraham Hernandez Medina
int main(){
   float ladoA;
   float ladoB;
   float ladoC;
   float perimetro;
   
   printf("Ingresa la medida del lado 1 del triangulo escaleno: ");
   scanf("%f", &ladoA);
   
   printf("Ingresa la medida del lado 2 del triangulo escaleno: ");
   scanf("%f", &ladoB);
   
   printf("Ingresa la medida del lado 3 del triangulo escaleno: ");
   scanf("%f", &ladoC);
   
   perimetro = ladoA + ladoB + ladoC;
   
   printf("El perimetro es %f\n", perimetro);
   return 0;
}